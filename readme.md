# altindex

For [altcointrading.net](https://altcointrading.net), working prototype. Check `license.md`.

This backend script uses bundler.
Configuration in `lib/config.yaml`, coins to calculate with are set via an array in `main.rb` for now

```
bundle install
ruby main.rb
```

## Ticker with Vue 2

You can reuse this Winkdex vue app:

```
<body>

  <script src="https://unpkg.com/vue/dist/vue.js"></script><!-- 2.2.1 -->
  <script src="https://cdn.jsdelivr.net/vue.resource/1.0.3/vue-resource.min.js"></script>

  <script src="moment.js" charset="utf-8"></script>

    <div class="datablock">
      <div id="winkdex">
        <p>$ {{ $data.gridData.price | dollars }}</p>
        <small>{{ $data.gridData.timestamp | moment }}</small>
      </div>
    </div>




  <script type="text/javascript">

  Vue.use(VueResource)


  var winkdex = new Vue({

      el: '#winkdex',
      data: {
        gridColumns: ['total_market_cap_usd', 'bitcoin_percentage_of_market_cap'],
        gridData: [
        {
          "price": "-",
          "timestamp": "-"
        }
        ]
      },
      filters: {
        moment: function(str) {
          var d = new Date(str);
          if (moment(d).isValid() ) {
            return moment(d).format('MMMM Do \'YY, h:mm a') + " UTC";
          } else {
            return "-";
          }
        },
        dollars: function(str) {
          var dp = str/100
          if (isNaN(dp)) {
            return "-"
          } else {
            return dp
          }
        }
      },
      mounted: function() {
        var vm = this
        vm.$http.get("https://thisisgentlemen.net/winkdex/response.json")
          .then(function (response) {
            vm.gridData = response.data
          })
          .catch(function (error) {
            vm.gridData = error
          })
        setInterval(function() {
          vm.$http.get("https://thisisgentlemen.net/winkdex/response.json")
            .then(function (response) {
              vm.gridData = response.data
            })
        }, 10000)

      }
  })

  </script>


  <style media="screen">
    body {
        margin: 0
    }
    #winkdex {
      display: block;
      color: #3c3b3b;
      font-family: "Raleway", Helvetica, sans-serif;
      font-weight: 800;
      letter-spacing: 0.05em;
      line-height: 1;
      margin: 0;
      text-transform: uppercase;
      font-size: 42px;
      transition: all 1s;
    }
    #winkdex p {
      margin: 0 0 25px 0;
    }
    #winkdex small {
      font-weight: 200;
      font-size: 13px;
      letter-spacing: 3.2px;
    }
  </style>


</body>

```

## Charting with chartist.js

Something liek this

```

<script type="text/javascript">

function loadJSON(tocall, callback) {
    var xobj = new XMLHttpRequest();
      xobj.overrideMimeType("application/json");
      xobj.open('GET', tocall, true);
      xobj.onreadystatechange = function () {
      if (xobj.readyState == 4 && xobj.status == "200") {
        callback(xobj.responseText);
      }
    };
    xobj.send(null);
}

function init( tocall, options, chartclass ) {
 loadJSON(tocall, function(response) {
    var actual_JSON = JSON.parse(response);
    var labels = []
    var series = []
    for (index = 0; index < actual_JSON.length; ++index) {
      labels.push(actual_JSON[index]['timestamp']);
      series.push(  {meta: moment(actual_JSON[index]['timestamp']).format("MMM Do YYYY"), value: actual_JSON[index]['price']}  ); //series
      //series.push(actual_JSON[index]['price'])
    }
    var data = { labels: labels.reverse(), series: [series.reverse()] };

    new Chartist.Line( chartclass, data, options);

 });
}

var options = {
  showPoint: true,
  lineSmooth: true,
  axisX: {
    showGrid: false,
    showLabel: true,
    labelInterpolationFnc: function skipLabels(value, index) {
     return index % 50  === 0 ? moment(value).format("MMM D YY") : null;
    }
  },
  axisY: {
    type: Chartist.AutoScaleAxis,
    offset: 5,
    labelInterpolationFnc: function(value) {
      return value;
    }
  },
  plugins: [
   Chartist.plugins.tooltip({
     class: 'tooltipclass',
     appendToBody: true
   })
 ]
};

init( 'https://raw.githubusercontent.com/altcointrading/vol/master/bvol500.json', options, '#bvol1' );

</script>

```

## Charting with Vue 2

https://github.com/apertureless/vue-chartjs

## amp-friendly

embed the ticker/chart as `amp-iframe`
