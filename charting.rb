require_relative "lib/cmc.rb"
include CMC

coinarray = ["ethereum", "ethereum-classic", "dash", "monero", "litecoin", "augur", "factom", "zcash", "ripple" ]
CMC::Advanced.new(coinarray).worker
