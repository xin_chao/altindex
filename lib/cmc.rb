module CMC

  extend self

  require "net/http"
  require "net/https"
  require "uri"
  require "json"
  require "yaml"
  require "mongo"
  require "pp"

  private
  def db_con
    begin
      cdata = config
      client = Mongo::Client.new([ cdata[0] ],user: cdata[1],password: cdata[2],:database => cdata[3])
    rescue
    	puts "Error connecting to database."
    end
   return client
  end

  private
  def config
      conf = YAML::load_file(  File.join(__dir__, 'config.yaml')  )['mongo']
      cdata = [conf['server'],conf['user'],conf['pass'],conf['db']]
    return cdata
  end

  def curl i
    url = URI.parse( i )
    headers = {"User-Agent" => "ruby:cmc v0.0.1 by altcointrading.net" }
    https = Net::HTTP.new(url.host,url.port)
    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE
    req = Net::HTTP::Get.new(url.request_uri, headers)
    res = https.request(req)
    data = JSON.parse( res.body )
    return data
  end

  class Master

    attr_reader :coinarray
    attr_accessor :confare, :dumpslug

    def initialize coinarray
      @coinarray = coinarray
      @confarea = YAML::load_file( File.join(__dir__, 'config.yaml') )
      #@basepwd = YAML::load_file( File.join(__dir__, 'config.yaml') )['basepwd']
      @dumpslug = YAML::load_file( File.join(__dir__, 'config.yaml') )['dumpslug']
    end

    def worker
     "$#{setter}"
    end


    def setter
      raise NotImplementedError, " method not implemented in #{self.class}"
    end


  end

  class Index < Master

    def setter
      # output todays data as a public json
      bulkdata = self.getter

      # store todays data in a db
      # xx

      # calculate index
      # create public index json for chart
      self.calculate bulkdata

      # store index in a db
      # xx
    end
    def getter
      #puts "Coins to index: #{@coinarray}"
      bulkdata = {}
      @coinarray.each do |coin|
        call = "https://api.coinmarketcap.com/v1/ticker/#{coin}/"
        url = URI.parse( call )
        headers = {"User-Agent" => "ruby:cmc v0.0.1 by altcointrading.net" }
         https = Net::HTTP.new(url.host,url.port)
         https.use_ssl = true
         https.verify_mode = OpenSSL::SSL::VERIFY_NONE
        req = Net::HTTP::Get.new(url.request_uri, headers)
        res = https.request(req)
          data = JSON.parse( res.body )
          bulkdata["#{coin}"] = data
      end
      file = "#{@dumpslug}" + "bulkdata.json"
        jf = open( file, 'w+')
        jf.write( bulkdata.to_json )
        jf.close

        p "Closed #{file}"
      return bulkdata
    end

    # math helpers

    def calculate bulkdata

      rawindex = {}
      totalvol = 0
      weight = 0
      index = 0

      @coinarray.each do |coin|
        vals = bulkdata["#{coin}"][0]
        totalvol = totalvol + vals["24h_volume_usd"].to_f
        rawindex["#{coin}"] = { "price" => vals["price_btc"].to_f , "vol" => vals["24h_volume_usd"].to_f }
      end
      @coinarray.each do |coin|
        weight = rawindex["#{coin}"]["price"] * rawindex["#{coin}"]["vol"] / totalvol
        index = index + weight
      end

      rawindex["index"] = index

      file = "#{@dumpslug}" + "rawindex.json"
        jf = open( file, 'w+')
        jf.write( rawindex.to_json )
        jf.close

      indexonly = [ rawindex["index"], Time.now ]
      file = "#{@dumpslug}" + "index.json"
        jf = open( file, 'w+')
        jf.write( indexonly.to_json )
        jf.close

      cl = db_con
      col = cl[:cmcbeta]
        if col.count < 1 then col.create end
      col.insert_one( { "index" => rawindex["index"], "time" => Time.now } )

      return rawindex
    end

  end#class Index

  class Charting < Master
    def setter

      cl = db_con
      col = cl[:cmcbeta]

      documents = col.find() #.skip(col.count() - 100)
      outer = []

      documents.each do |document|
       inner = {
        :index => document["index"],
        :time => document["time"]
        }
        outer.push( inner )
      end

      outer.reverse!

      p " == Outer: "
      p outer

      if outer.count>100 then outer = outer[0..99] end
      all = { :data => outer }

      file = @dumpslug + "chartdata.json"
        jf = open( file, 'w+')
        jf.write( all.to_json )
        jf.close

    end
  end#class Charting

  class Advanced < Master

        def setter
          bulkdata = self.getter
          self.calculate bulkdata
          self.chartdata
        end

        def getter
          bulkdata = {}
          @coinarray.each do |coin|
            call = "https://api.coinmarketcap.com/v1/ticker/#{coin}/"
            url = URI.parse( call )
            headers = {"User-Agent" => "ruby:cmc v0.0.1 by altcointrading.net" }
             https = Net::HTTP.new(url.host,url.port)
             https.use_ssl = true
             https.verify_mode = OpenSSL::SSL::VERIFY_NONE
            req = Net::HTTP::Get.new(url.request_uri, headers)
            res = https.request(req)
              data = JSON.parse( res.body )
              bulkdata["#{coin}"] = data
          end
          return bulkdata
        end

        # math helpers

        def calculate bulkdata

          rawindex = {}
          voldata = {}
          totalvol = 0
          weight = 0
          index = 0

          @coinarray.each do |coin|
            vals = bulkdata["#{coin}"][0]
            totalvol = totalvol + vals["24h_volume_usd"].to_f
            rawindex["#{coin}"] = vals["price_btc"].to_f
            voldata["#{coin}"] = vals["24h_volume_usd"].to_f
          end
          @coinarray.each do |coin|
            weight = rawindex["#{coin}"] * voldata["#{coin}"] / totalvol
            index = index + weight
          end

          rawindex["index"] = index
          rawindex["time"] = Time.now

          #pp rawindex

          cl = db_con
          col = cl[:advancedbeta]
            if col.count < 1 then col.create end
          col.insert_one( rawindex )

          return rawindex
        end

        def chartdata
          cl = db_con
          col = cl[:advancedbeta]

          documents = col.find()
          outer = []

          documents.each do |document|
           inner = {
            :index => document["index"],
            :time => document["time"],
            :eth=>document["ethereum"],
            :xmr=>document["monero"],
            :dash=>document["dash"],
            :zec=>document["zcash"]
            }
            outer.push( inner )
          end

          #outer.reverse!

          if outer.count>100 then outer = outer[0..99] end
          all = { :data => outer }

          file = @dumpslug + "advanced.json"
            jf = open( file, 'w+')
            jf.write( all.to_json )
            jf.close
        end

  end#class Advanced

  class FundamentalsEth < Master
    def setter
      self.get_txdaily
      self.get_exchangedaily
    end

    def get_txdaily

      # get tx number total, retrieve last, subtract
      data = curl "https://etherchain.org/api/txs/count"

      # total count
      txtotal = data["data"][0]['count']

      # find from db
      cl = db_con
      col = cl[:txdaily]
      if col.count < 1 then col.create end

      lastentry = col.find().sort({_id:-1}).limit(1)
      day = txtotal.to_i - lastentry["total"].to_i

      col.insert_one {"time" => Time.now, "daily" => day, "total" => txtotal}


      # dump datapoints
      documents = col.find()
      outer = []
      documents.each do |document|
       inner = {
        :time => document["time"],
        :daily => document["daily"],
        :total => document["total"]
        }
        outer.push( inner )
      end
      if outer.count>100 then outer = outer[0..99] end
      all = { :data => outer }
      file = @dumpslug + "txdaily.json"
      jf = open( file, 'w+')
      jf.write( all.to_json )
      jf.close
    end

    def get_exchangedaily
      #get daily exchange vol from cmc  == those can be compared in growth maybe olay chart but not more
      data = curl "https://www.cryptocompare.com/api/data/coinsnapshot/?fsym=ETH&tsym=USD"

      cl = db_con
      col = cl[:exchangedaily]
      if col.count < 1 then col.create end

      col.insert_one {
        "time" => Time.now,
        "vol24usd" => data["Data"]["AggregatedData"]["VOLUME24HOURTO"].to_i,
        "price" => data["Data"]["AggregatedData"]["PRICE"].to_f
      }

      # dump datapoints
      documents = col.find()
      outer = []
      documents.each do |document|
       inner = {
        :time => document["time"],
        :vol24usd => document["vol24usd"],
        :price => document["price"]
        }
        outer.push( inner )
      end
      if outer.count>100 then outer = outer[0..99] end
      all = { :data => outer }
      file = @dumpslug + "exchangedaily.json"
      jf = open( file, 'w+')
      jf.write( all.to_json )
      jf.close

    end
  end#class FundamentalsEth

  class Backup
    def setter
      # db dump
      # webdav yandex
    end
  end#class Backup

end#module CMC
